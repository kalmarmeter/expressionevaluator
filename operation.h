#ifndef OPERATION_H_INCLUDED
#define OPERATION_H_INCLUDED

struct Operation {
    Operation() {}
    virtual int operator() (int op1) {return 0;}
    virtual int operator() (int op1, int op2) {return 0;}
    virtual ~Operation() {}
};

struct Add : Operation {
    Add() {}
    int operator() (int op1, int op2) override final {
        return op1 + op2;
    }
};

struct Sub : Operation {
    Sub() {}
    int operator() (int op1, int op2) override final {
        return op1 - op2;
    }
};

struct Mult : Operation {
    Mult() {}
    int operator() (int op1, int op2) override final {
        return op1 * op2;
    }
};

struct Div : Operation {
    Div() {}
    int operator() (int op1, int op2) override final {
        return op1 / op2;
    }
};

struct Negate : Operation {
    Negate() {}
    int operator() (int op1) override final {
        return -op1;
    }
};

struct Greater : Operation {
    Greater() {}
    int operator() (int op1, int op2) override final {
        return (op1 - op2 > 0);
    }
};

struct GreaterEq : Operation {
    GreaterEq() {}
    int operator() (int op1, int op2) override final {
        return (op1 - op2 >= 0);
    }
};

struct Less : Operation {
    Less() {}
    int operator() (int op1, int op2) override final {
        return (op1 - op2 < 0);
    }
};

struct LessEq : Operation {
    LessEq() {}
    int operator() (int op1, int op2) override final {
        return (op1 - op2 <= 0);
    }
};

struct Equals : Operation {
    Equals() {}
    int operator() (int op1, int op2) override final {
        return (op1 - op2 == 0);
    }
};

#endif // OPERATION_H_INCLUDED
