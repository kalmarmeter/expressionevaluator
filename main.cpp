#include "evaluator.h"

int main()
{
    Evaluator ev(true);

    bool quit = false;
    bool debug = false;

    while (!quit) {

        std::cout << "======================================================================================" << std::endl;
        std::cout << " Specify the name of program file to compile, use \"debug\" to turn debug mode on-off," << std::endl;
        std::cout << "or use \"quit\" to stop the compiler" << std::endl;
        std::cout << "======================================================================================" << std::endl << std::endl;

        std::string in;
        std::cin >> in;
        if (in=="quit") {
            quit = true;
            continue;
        } else if (in=="debug") {
            debug = !debug;
            std::cout << "Debug mode is now turned " << (debug?"on":"off") << std::endl;
            continue;
        }
        ev.fromFile(in, debug);
        std::cout << std::endl;
    }

    return 0;
}
