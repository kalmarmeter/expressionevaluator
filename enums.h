#ifndef ENUMS_H_INCLUDED
#define ENUMS_H_INCLUDED

enum class CharType {
    Letter, Digit, OpenPar, ClosePar, Operator
};

enum class TokenType {
    Variable, Integer, OpenPar, ClosePar, Operator
};

enum class LineType {
    Expression, LoopBegin, LoopEnd, Print, PrintLn, NewLn, Space, IfBegin, IfEnd
};

enum class EvException {
    NoFile,
    NoVariable,
    NoOpenPar, NoClosePar,
    NoOperator,
    NoChar,
    FewOperands,
    AssignError,
    BadControl,
    NoLoopBegin, NoLoopEnd,
    NoIfBegin, NoIfEnd,
    ArrayNegativeSize, ArrayIndexError, ArrayOpError,
    Other
};

#endif // ENUMS_H_INCLUDED
