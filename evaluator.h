#ifndef EVALUATOR_H_INCLUDED
#define EVALUATOR_H_INCLUDED

#include <iostream>
#include <fstream>
#include <map>
#include <stack>
#include <vector>
#include <string>
#include <sstream>
#include <algorithm>

#include "operatorattributes.h"
#include "enums.h"
#include "tostring.h"

struct Token {
    std::string value;
    TokenType type;

    Token(std::string s, TokenType t) : value(s), type(t) {}
};

struct Line {
    LineType type;
    std::vector<Token> lineTokens;

    Line(LineType t, std::vector<Token> v) : type(t), lineTokens(v) {}
};

class Evaluator {
private:
    std::vector<char> operatorSymbols;
    std::map<std::string,OperatorAttributes> operatorAttrs;
    std::map<std::string,int> variables;
    std::vector<Line> lines;
    std::vector<std::pair<int,int>> loops;
    std::vector<std::pair<int,int>> conditionals;

    CharType getCharType(char);
    TokenType getTokenType(CharType);
    int getIntValue(const std::string&);


public:
    Evaluator(bool);

    Evaluator& setVariable(std::string,int);
    Evaluator& setOperator(std::string,OperatorAttributes);


private:
    std::vector<std::string> splitString(const std::string&,char);
    std::string getLineType(const std::string&);
    std::string removeWhitespace(const std::string&);
    std::vector<Token> tokenizeString(const std::string&);
    std::vector<Token> getPolishExpression(const std::vector<Token>&);
    int evaluateExpression(const std::vector<Token>&);

    void printException(EvException);
    void printVariables();
    void printPolishLines();
    void printLoops();
    void printConditionals();


public:
    void fromFile(const std::string&, bool);
    void evaluateLines(const std::string&);
    int evaluateLine(const std::string&);

    ~Evaluator();


private:
    static std::map<std::string,OperatorAttributes> baseOperators;
};

#endif // EVALUATOR_H_INCLUDED
