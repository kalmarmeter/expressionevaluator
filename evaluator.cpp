#include "evaluator.h"

std::map<std::string,OperatorAttributes> Evaluator::baseOperators = {
    {">", OperatorAttributes(0, 2, new Greater)},
    {">=", OperatorAttributes(0, 2, new GreaterEq)},
    {"<", OperatorAttributes(0, 2, new Less)},
    {"<=", OperatorAttributes(0, 2, new LessEq)},
    {"==", OperatorAttributes(0, 2, new Equals)},

    {"::", OperatorAttributes(4, 2, nullptr)},
    {"=", OperatorAttributes(0, 2, nullptr)},

    {"+", OperatorAttributes(1, 2, new Add)},
    {"-", OperatorAttributes(1, 2, new Sub)},
    {"*", OperatorAttributes(2, 2, new Mult)},
    {"/", OperatorAttributes(2, 2, new Div)},

    {"-.", OperatorAttributes(3, 1, new Negate)}
};

Evaluator::Evaluator(bool useDefaultOpAttributes) {
    if (useDefaultOpAttributes) {
        operatorAttrs = baseOperators;
    }
    operatorSymbols = {'+','-','*','/','>','<','=',':'};
}


CharType Evaluator::getCharType(char c) {
    if ((c>=65 && c<=90) || (c>=97 && c<=122) || c=='_') {
        return CharType::Letter;
    }
    if (c>=48 && c<=57) {
        return CharType::Digit;
    }
    if (c=='(') {
        return CharType::OpenPar;
    }
    if (c==')') {
        return CharType::ClosePar;
    }
    if (std::find(operatorSymbols.begin(), operatorSymbols.end(), c) != operatorSymbols.end()) {
        return CharType::Operator;
    }
    throw EvException::NoChar;
}

TokenType Evaluator::getTokenType(CharType cType) {
    TokenType result;
    switch (cType) {
    case CharType::Letter:
        result = TokenType::Variable;
        break;
    case CharType::Digit:
        result = TokenType::Integer;
        break;
    case CharType::OpenPar:
        result = TokenType::OpenPar;
        break;
    case CharType::ClosePar:
        result = TokenType::ClosePar;
        break;
    case CharType::Operator:
        result = TokenType::Operator;
        break;
    }
    return result;
}

std::string Evaluator::getLineType(const std::string& in) {
    std::string ctrl;
    if (in.size() > 1) {
        if (in.at(0) == '_') {
            ctrl.push_back('_');
            int i = 1;
            while (i<in.size() && in.at(i) != '_') {
                ctrl.push_back(in.at(i));
                ++i;
            }
            if (i<in.size()) {
                ctrl.push_back('_');
            } else {
                throw EvException::BadControl;
            }
        }
    }
    return ctrl;
}

int Evaluator::getIntValue(const std::string& in) {
    std::stringstream ss;
    ss << in;
    int result;
    ss >> result;
    return result;
}

Evaluator& Evaluator::setVariable(std::string name, int value) {
    variables[name] = value;
    return *this;
}

Evaluator& Evaluator::setOperator(std::string sym, OperatorAttributes prior) {
    operatorAttrs[sym] = prior;
    return *this;
}

std::vector<std::string> Evaluator::splitString(const std::string& in, char split) {
    std::vector<std::string> result;
    std::string line;
    for (char c : in) {
        if (c == split) {
            result.push_back(line);
            line = "";
        } else {
            line.push_back(c);
        }
    }
    return result;
}

std::string Evaluator::removeWhitespace(const std::string& in) {
    std::string result;
    for (char c : in) {
        if (c!=' ' && c!='\t' && c!='\n') {
            result.push_back(c);
        }
    }
    return result;
}

std::vector<Token> Evaluator::tokenizeString(const std::string& in) {
    std::vector<Token> result;
    int i = 0;
    while (i<in.size()) {
        CharType chType = getCharType(in.at(i));
        TokenType tType = getTokenType(chType);

        int j = i;
        std::string val;
        while (j<in.size() && getCharType(in.at(j)) == chType) {
            val.push_back(in.at(j));
            if (tType != TokenType::Integer && tType != TokenType::Variable && tType != TokenType::Operator) {
                ++j;
                break;
            }
            ++j;
        }
        if (tType == TokenType::Operator && val == "-") {
            if (result.size() == 0 || (result.back().type != TokenType::Variable && result.back().type != TokenType::Integer && result.back().type != TokenType::ClosePar)) {
                val = "-.";
            }
        }
        result.push_back(Token(val,tType));
        i = j;
    }
    return result;
}

std::vector<Token> Evaluator::getPolishExpression(const std::vector<Token>& in) {
    std::stack<Token> stck;
    std::vector<Token> result;
    for (Token t : in) {
        switch (t.type) {
        case TokenType::Variable:
            result.push_back(t);
            break;
        case TokenType::Integer:
            result.push_back(t);
            break;
        case TokenType::OpenPar:
            stck.push(t);
            break;
        case TokenType::Operator:
            while (!stck.empty() && operatorAttrs[t.value].priority <= operatorAttrs[stck.top().value].priority) {
                result.push_back(stck.top());
                stck.pop();
            }
            stck.push(t);
            break;
        case TokenType::ClosePar:
            while (!stck.empty() && stck.top().type != TokenType::OpenPar) {
                result.push_back(stck.top());
                stck.pop();
            }
            if(!stck.empty()) {
                stck.pop();
            } else {
                throw EvException::NoOpenPar;
            }
            break;
        }
    }
    while (!stck.empty()) {
        if (stck.top().type == TokenType::OpenPar) {
            throw EvException::NoClosePar;
        }
        result.push_back(stck.top());
        stck.pop();
    }
    return result;
}
int Evaluator::evaluateExpression(const std::vector<Token>& in) {
    std::stack<Token> stck;
    for (Token t : in) {
        switch (t.type) {
        case TokenType::Variable:
            stck.push(t);
            break;
        case TokenType::Integer:
            stck.push(t);
            break;
        case TokenType::Operator:
            int result = 0;
            if (operatorAttrs.find(t.value) == operatorAttrs.end()) {
                throw EvException::NoOperator;
            }
            if (t.value == "=") {


                int op2;
                if (!stck.empty()) {
                    if (stck.top().type == TokenType::Integer) {
                        op2 = getIntValue(stck.top().value);
                    } else if (stck.top().type == TokenType::Variable) {
                        if (variables.find(stck.top().value) == variables.end()) {
                            throw EvException::NoVariable;
                        }
                        op2 = variables[stck.top().value];
                    } else {
                        throw EvException::AssignError;
                    }
                    stck.pop();
                } else {
                    throw EvException::FewOperands;
                }
                if (!stck.empty()) {
                    if (stck.top().type == TokenType::Variable) {
                        variables[stck.top().value] = op2;
                    } else {
                        throw EvException::AssignError;
                    }
                    stck.pop();
                } else {
                    throw EvException::FewOperands;
                }
                stck.push(Token(patch::to_string(op2),TokenType::Integer));


            } else if (t.value=="::") {

                std::string varName;
                bool declSucc = false;
                bool indSucc = false;
                int op2;
                if (!stck.empty()) {
                    if (stck.top().type == TokenType::Integer) {
                        op2 = getIntValue(stck.top().value);
                    } else if (stck.top().type == TokenType::Variable) {
                        if (variables.find(stck.top().value) == variables.end()) {
                            throw EvException::NoVariable;
                        }
                        op2 = variables[stck.top().value];
                    } else {
                        throw EvException::ArrayOpError;
                    }
                    stck.pop();
                } else {
                    throw EvException::FewOperands;
                }
                if (!stck.empty()) {
                    if (stck.top().type == TokenType::Variable) {
                        std::string arrayName = stck.top().value;
                        if (variables.find(arrayName+"$"+"0") == variables.end()) {
                            if (op2 > 0) {
                                for (int i=0; i<op2; ++i) {
                                    std::string arrayIndexedName = arrayName + "$" + patch::to_string(i);
                                    variables[arrayIndexedName] = 0;
                                }
                                declSucc = true;
                            } else {
                                throw EvException::ArrayNegativeSize;
                            }
                        } else {
                            varName = stck.top().value + "$" + patch::to_string(op2);
                            if (variables.find(varName) != variables.end()) {
                                indSucc = true;
                            } else {
                                throw EvException::ArrayIndexError;
                            }
                        }
                    } else {
                        throw EvException::ArrayOpError;
                    }
                    stck.pop();
                } else {
                    throw EvException::FewOperands;
                }

                if (indSucc) {
                    stck.push(Token(varName, TokenType::Variable));
                } else if (declSucc) {
                    stck.push(Token("0", TokenType::Integer));
                }


            } else {


                if (operatorAttrs[t.value].opNumber == 1) {
                    int op1;
                    if (!stck.empty()) {
                        if (stck.top().type == TokenType::Integer) {
                            op1 = getIntValue(stck.top().value);
                        } else if (stck.top().type == TokenType::Variable) {
                            if (variables.find(stck.top().value) == variables.end()) {
                                throw EvException::NoVariable;
                            }
                            op1 = variables[stck.top().value];
                        }
                        stck.pop();
                    } else {
                        throw EvException::FewOperands;
                    }
                    result = (*operatorAttrs[t.value].operation)(op1);
                } else if (operatorAttrs[t.value].opNumber == 2) {
                    int op1, op2;
                    if (!stck.empty()) {
                        if (stck.top().type == TokenType::Integer) {
                            op2 = getIntValue(stck.top().value);
                            stck.pop();
                        } else if (stck.top().type == TokenType::Variable) {
                            if (variables.find(stck.top().value) == variables.end()) {
                                throw EvException::NoVariable;
                            }
                            op2 = variables[stck.top().value];
                            stck.pop();
                        }
                    } else {
                        throw EvException::FewOperands;
                    }
                    if (!stck.empty()) {
                        if (stck.top().type == TokenType::Integer) {
                            op1 = getIntValue(stck.top().value);
                            stck.pop();
                        } else if (stck.top().type == TokenType::Variable) {
                            if (variables.find(stck.top().value) == variables.end()) {
                                throw EvException::NoVariable;
                            }
                            op1 = variables[stck.top().value];
                            stck.pop();
                        }
                    } else {
                        throw EvException::FewOperands;
                    }
                    result = (*operatorAttrs[t.value].operation)(op1,op2);
                }
                stck.push(Token(patch::to_string(result),TokenType::Integer));


            }
        }
    }
    if (stck.size() == 0) {
        return 0;
    }
    if (stck.size() > 1) {
        throw EvException::Other;
    }
    if (stck.top().type == TokenType::Variable) {
        return variables[stck.top().value];
    } else {
        return getIntValue(stck.top().value);
    }
}

void Evaluator::printException(EvException ex) {
    std::cerr << "EXCEPTION: ";
    switch (ex) {
    case EvException::FewOperands:
        std::cerr << "Too few operands for an operator";
        break;
    case EvException::NoClosePar:
        std::cerr << "No closing paranthesis found for an opening one";
        break;
    case EvException::NoOpenPar:
        std::cerr << "No opening paranthesis found for a closing one";
        break;
    case EvException::NoVariable:
        std::cerr << "Variable not found";
        break;
    case EvException::NoOperator:
        std::cerr << "Operator not found";
        break;
    case EvException::NoChar:
        std::cerr << "Unrecognized character";
        break;
    case EvException::AssignError:
        std::cerr << "Error at assignment";
        break;
    case EvException::BadControl:
        std::cerr << "Bad control statement, expected closing _";
        break;
    case EvException::NoLoopBegin:
        std::cerr << "No loop beginning found";
        break;
    case EvException::NoLoopEnd:
        std::cerr << "No loop end found";
        break;
    case EvException::NoIfBegin:
        std::cerr << "No if beginning found";
        break;
    case EvException::NoIfEnd:
        std::cerr << "No if end found";
        break;
    case EvException::NoFile:
        std::cerr << "File not found";
        break;
    case EvException::ArrayIndexError:
        std::cerr << "Array index out of bounds";
        break;
    case EvException::ArrayNegativeSize:
        std::cerr << "Array size is negative";
        break;
    case EvException::ArrayOpError:
        std::cerr << "Error at array declaration";
        break;
    case EvException::Other:
        std::cerr << "Other exception";
        break;
    }
    std::cerr << std::endl;
}

int Evaluator::evaluateLine(const std::string& in) {
    try {
        std::string inWs = removeWhitespace(in);
        std::vector<Token> tokenVec = tokenizeString(inWs);
        std::vector<Token> polishVec = getPolishExpression(tokenVec);
        int result = evaluateExpression(polishVec);
        return result;
    } catch (EvException ex) {
        printException(ex);
    }
}

void Evaluator::evaluateLines(const std::string& in) {
    try {
        lines.clear();
        loops.clear();
        conditionals.clear();
        variables.clear();
        std::vector<std::string> lineStr = splitString(in,';');
        std::vector<int> loopStartPos;
        std::vector<int> condStartPos;
        int lineNum = 0;
        for (std::string s : lineStr) {
            std::string noWs = removeWhitespace(s);
            std::string ctrl = getLineType(noWs);

            LineType type;

            if (ctrl=="_LOOP_") {
                type = LineType::LoopBegin;
                loopStartPos.push_back(lineNum);
            } else if (ctrl=="_ENDLOOP_") {
                type = LineType::LoopEnd;
                if (loopStartPos.empty()) {
                    throw EvException::NoLoopBegin;
                }
                int start = loopStartPos.back();
                loops.push_back(std::make_pair(start, lineNum));
                loopStartPos.pop_back();
            } else if (ctrl=="_IF_") {
                type = LineType::IfBegin;
                condStartPos.push_back(lineNum);
            } else if (ctrl=="_ENDIF_") {
                type = LineType::IfEnd;
                if (condStartPos.empty()) {
                    throw EvException::NoIfBegin;
                }
                int start = condStartPos.back();
                conditionals.push_back(std::make_pair(start, lineNum));
                condStartPos.pop_back();
            } else if (ctrl=="_PRINTLN_") {
                type = LineType::PrintLn;
            } else if (ctrl=="_PRINT_") {
                type = LineType::Print;
            } else if (ctrl=="_NEWLN_") {
                type = LineType::NewLn;
            } else if (ctrl=="_SPACE_") {
                type = LineType::Space;
            } else if (ctrl=="") {
                type = LineType::Expression;
            }

            noWs.erase(0,ctrl.size());

            std::vector<Token> tokenVec = tokenizeString(noWs);
            std::vector<Token> polishVec = getPolishExpression(tokenVec);

            lines.push_back(Line(type, polishVec));

            ++lineNum;
        }
        if (!loopStartPos.empty()) {
            throw EvException::NoLoopEnd;
        }
        if (!condStartPos.empty()) {
            throw EvException::NoIfEnd;
        }

        int i = 0;
        while (i<lines.size()) {
            int result = evaluateExpression(lines[i].lineTokens);
            bool cond;
            switch (lines[i].type) {
            case LineType::Expression:
                ++i;
                break;
            case LineType::PrintLn:
                std::cout << result << std::endl;
                ++i;
                break;
            case LineType::Print:
                std::cout << result;
                ++i;
                break;
            case LineType::NewLn: {
                int num = ((result<=0)?1:result);
                for (int j=0; j<num; ++j) {
                    std::cout << std::endl;
                }
                ++i;
                break;
            }
            case LineType::Space: {
                int num = ((result<=0)?1:result);
                for (int j=0; j<num; ++j) {
                    std::cout << " ";
                }
                ++i;
                break;
            }
            case LineType::LoopBegin:
                cond = result;
                if (cond) {
                    ++i;
                } else {
                    int j;
                    for(j=0;j<loops.size();++j) {
                        if (loops[j].first == i) {
                            break;
                        }
                    }
                    i = loops[j].second + 1;
                }
                break;
            case LineType::LoopEnd:
                int j;
                for(j=0;j<loops.size();++j) {
                    if (loops[j].second == i) {
                        break;
                    }
                }
                i = loops[j].first;
                break;
            case LineType::IfBegin:
                cond = result;
                if (cond) {
                    ++i;
                } else {
                    for(j=0;j<conditionals.size();++j) {
                        if (conditionals[j].first == i) {
                            break;
                        }
                    }
                    i = conditionals[j].second + 1;
                }
                break;
            case LineType::IfEnd:
                ++i;
                break;
            }
        }
    } catch (EvException ex) {
        printException(ex);
    }
}

void Evaluator::fromFile(const std::string& fname, bool debug) {
    try {
        std::cout << "Reading from file: " << fname << std::endl;
        std::ifstream file(fname.c_str());
        if (file.fail()) {
            throw EvException::NoFile;
        }
        std::string str;
        while (!file.eof()) {
            std::string s;
            std::getline(file, s);
            str += s;
        }
        file.close();
        std::cout << "Reading successful, executing..." << std::endl;
        std::cout << "-------------------------------" << std::endl << std::endl << std::endl;
        evaluateLines(str);
        std::cout << std::endl << std::endl;
        std::cout << "-------------------------------" << std::endl << "Execution finished" << std::endl;

        if(debug) {
            std::cout << std::endl;
            std::cout << "###################################" << std::endl;
            std::cout << "DEBUG INFORMATION" << std::endl;
            std::cout << "-----------------------------------" << std::endl;
            printPolishLines();
            printVariables();
            printConditionals();
            printLoops();
            std::cout << "###################################" << std::endl;
        }


    } catch (EvException ex) {
        printException(ex);
    }
}

void Evaluator::printVariables() {
    std::cout << "Variables" << std::endl;
    std::cout << "-----------------------------------" << std::endl;
    for (auto it = variables.begin(); it !=variables.end(); ++it) {
        std::cout << "\t" << it->first << " === " << it->second << std::endl;
    }
    std::cout << "-----------------------------------" << std::endl;
}

void Evaluator::printPolishLines() {
    std::cout << "Lines in polish form" << std::endl;
    std::cout << "-----------------------------------" << std::endl;
    for (auto l : lines) {
        std::cout << "\t";
        for (auto t : l.lineTokens) {
            std::cout << t.value << "__";
        }
        std::cout << std::endl;
    }
    std::cout << "-----------------------------------" << std::endl;
}

void Evaluator::printLoops() {
    std::cout << "Loop begin and end positions" << std::endl;
    std::cout << "-----------------------------------" << std::endl;
    for (auto p : loops) {
        std::cout << "\tfrom " << p.first << " to " << p.second << std::endl;
    }
    std::cout << "-----------------------------------" << std::endl;
}

void Evaluator::printConditionals() {
    std::cout << "Conditional begin and end positions" << std::endl;
    std::cout << "-----------------------------------" << std::endl;
    for (auto p : conditionals) {
        std::cout << "\tfrom " << p.first << " to " << p.second << std::endl;
    }
    std::cout << "-----------------------------------" << std::endl;
}

Evaluator::~Evaluator() {
    for (auto i = operatorAttrs.begin(); i != operatorAttrs.end(); ++i) {
        delete i->second.operation;
    }
}
