#ifndef TOSTRING_H_INCLUDED
#define TOSTRING_H_INCLUDED

#include <string>
#include <sstream>

namespace patch {

    template<typename T>
    std::string to_string(const T& in) {
        std::stringstream ss;
        ss << in;
        return ss.str();
    }
}

#endif // TOSTRING_H_INCLUDED
