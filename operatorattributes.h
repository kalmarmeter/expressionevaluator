#ifndef OPERATORATTRIBUTES_H_INCLUDED
#define OPERATORATTRIBUTES_H_INCLUDED

#include "operation.h"

struct OperatorAttributes {
    int priority;
    int opNumber;
    Operation* operation;

    OperatorAttributes() : priority(0), operation(nullptr) {}
    OperatorAttributes(int pr, int opNum, Operation* op) : priority(pr), opNumber(opNum), operation(op) {}
};

#endif // OPERATORATTRIBUTES_H_INCLUDED
